/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { StockTrackerTestModule } from '../../../test.module';
import { StockDailyTradeInfoDetailComponent } from '../../../../../../main/webapp/app/entities/stock-daily-trade-info/stock-daily-trade-info-detail.component';
import { StockDailyTradeInfoService } from '../../../../../../main/webapp/app/entities/stock-daily-trade-info/stock-daily-trade-info.service';
import { StockDailyTradeInfo } from '../../../../../../main/webapp/app/entities/stock-daily-trade-info/stock-daily-trade-info.model';

describe('Component Tests', () => {

    describe('StockDailyTradeInfo Management Detail Component', () => {
        let comp: StockDailyTradeInfoDetailComponent;
        let fixture: ComponentFixture<StockDailyTradeInfoDetailComponent>;
        let service: StockDailyTradeInfoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [StockTrackerTestModule],
                declarations: [StockDailyTradeInfoDetailComponent],
                providers: [
                    StockDailyTradeInfoService
                ]
            })
            .overrideTemplate(StockDailyTradeInfoDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(StockDailyTradeInfoDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StockDailyTradeInfoService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new StockDailyTradeInfo(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.stockDailyTradeInfo).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
