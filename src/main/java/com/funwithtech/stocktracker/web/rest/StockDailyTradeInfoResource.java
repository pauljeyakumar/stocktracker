package com.funwithtech.stocktracker.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.funwithtech.stocktracker.domain.StockDailyTradeInfo;

import com.funwithtech.stocktracker.repository.StockDailyTradeInfoRepository;
import com.funwithtech.stocktracker.repository.search.StockDailyTradeInfoSearchRepository;
import com.funwithtech.stocktracker.web.rest.errors.BadRequestAlertException;
import com.funwithtech.stocktracker.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing StockDailyTradeInfo.
 */
@RestController
@RequestMapping("/api")
public class StockDailyTradeInfoResource {

    private final Logger log = LoggerFactory.getLogger(StockDailyTradeInfoResource.class);

    private static final String ENTITY_NAME = "stockDailyTradeInfo";

    private final StockDailyTradeInfoRepository stockDailyTradeInfoRepository;

    private final StockDailyTradeInfoSearchRepository stockDailyTradeInfoSearchRepository;

    public StockDailyTradeInfoResource(StockDailyTradeInfoRepository stockDailyTradeInfoRepository, StockDailyTradeInfoSearchRepository stockDailyTradeInfoSearchRepository) {
        this.stockDailyTradeInfoRepository = stockDailyTradeInfoRepository;
        this.stockDailyTradeInfoSearchRepository = stockDailyTradeInfoSearchRepository;
    }

    /**
     * POST  /stock-daily-trade-infos : Create a new stockDailyTradeInfo.
     *
     * @param stockDailyTradeInfo the stockDailyTradeInfo to create
     * @return the ResponseEntity with status 201 (Created) and with body the new stockDailyTradeInfo, or with status 400 (Bad Request) if the stockDailyTradeInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/stock-daily-trade-infos")
    @Timed
    public ResponseEntity<StockDailyTradeInfo> createStockDailyTradeInfo(@Valid @RequestBody StockDailyTradeInfo stockDailyTradeInfo) throws URISyntaxException {
        log.debug("REST request to save StockDailyTradeInfo : {}", stockDailyTradeInfo);
        if (stockDailyTradeInfo.getId() != null) {
            throw new BadRequestAlertException("A new stockDailyTradeInfo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StockDailyTradeInfo result = stockDailyTradeInfoRepository.save(stockDailyTradeInfo);
        stockDailyTradeInfoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/stock-daily-trade-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /stock-daily-trade-infos : Updates an existing stockDailyTradeInfo.
     *
     * @param stockDailyTradeInfo the stockDailyTradeInfo to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated stockDailyTradeInfo,
     * or with status 400 (Bad Request) if the stockDailyTradeInfo is not valid,
     * or with status 500 (Internal Server Error) if the stockDailyTradeInfo couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/stock-daily-trade-infos")
    @Timed
    public ResponseEntity<StockDailyTradeInfo> updateStockDailyTradeInfo(@Valid @RequestBody StockDailyTradeInfo stockDailyTradeInfo) throws URISyntaxException {
        log.debug("REST request to update StockDailyTradeInfo : {}", stockDailyTradeInfo);
        if (stockDailyTradeInfo.getId() == null) {
            return createStockDailyTradeInfo(stockDailyTradeInfo);
        }
        StockDailyTradeInfo result = stockDailyTradeInfoRepository.save(stockDailyTradeInfo);
        stockDailyTradeInfoSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, stockDailyTradeInfo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /stock-daily-trade-infos : get all the stockDailyTradeInfos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of stockDailyTradeInfos in body
     */
    @GetMapping("/stock-daily-trade-infos")
    @Timed
    public List<StockDailyTradeInfo> getAllStockDailyTradeInfos() {
        log.debug("REST request to get all StockDailyTradeInfos");
        return stockDailyTradeInfoRepository.findAll();
        }

    /**
     * GET  /stock-daily-trade-infos/:id : get the "id" stockDailyTradeInfo.
     *
     * @param id the id of the stockDailyTradeInfo to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the stockDailyTradeInfo, or with status 404 (Not Found)
     */
    @GetMapping("/stock-daily-trade-infos/{id}")
    @Timed
    public ResponseEntity<StockDailyTradeInfo> getStockDailyTradeInfo(@PathVariable Long id) {
        log.debug("REST request to get StockDailyTradeInfo : {}", id);
        StockDailyTradeInfo stockDailyTradeInfo = stockDailyTradeInfoRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(stockDailyTradeInfo));
    }

    /**
     * DELETE  /stock-daily-trade-infos/:id : delete the "id" stockDailyTradeInfo.
     *
     * @param id the id of the stockDailyTradeInfo to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/stock-daily-trade-infos/{id}")
    @Timed
    public ResponseEntity<Void> deleteStockDailyTradeInfo(@PathVariable Long id) {
        log.debug("REST request to delete StockDailyTradeInfo : {}", id);
        stockDailyTradeInfoRepository.delete(id);
        stockDailyTradeInfoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/stock-daily-trade-infos?query=:query : search for the stockDailyTradeInfo corresponding
     * to the query.
     *
     * @param query the query of the stockDailyTradeInfo search
     * @return the result of the search
     */
    @GetMapping("/_search/stock-daily-trade-infos")
    @Timed
    public List<StockDailyTradeInfo> searchStockDailyTradeInfos(@RequestParam String query) {
        log.debug("REST request to search StockDailyTradeInfos for query {}", query);
        return StreamSupport
            .stream(stockDailyTradeInfoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
