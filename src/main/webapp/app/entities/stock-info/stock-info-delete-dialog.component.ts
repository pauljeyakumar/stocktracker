import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { StockInfo } from './stock-info.model';
import { StockInfoPopupService } from './stock-info-popup.service';
import { StockInfoService } from './stock-info.service';

@Component({
    selector: 'jhi-stock-info-delete-dialog',
    templateUrl: './stock-info-delete-dialog.component.html'
})
export class StockInfoDeleteDialogComponent {

    stockInfo: StockInfo;

    constructor(
        private stockInfoService: StockInfoService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.stockInfoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'stockInfoListModification',
                content: 'Deleted an stockInfo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-stock-info-delete-popup',
    template: ''
})
export class StockInfoDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private stockInfoPopupService: StockInfoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.stockInfoPopupService
                .open(StockInfoDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
