import { BaseEntity } from './../../shared';

export const enum Exchange {
    'NSE',
    'BSE'
}

export class Stock implements BaseEntity {
    constructor(
        public id?: number,
        public stockCode?: string,
        public stockName?: string,
        public exchange?: Exchange,
    ) {
    }
}
