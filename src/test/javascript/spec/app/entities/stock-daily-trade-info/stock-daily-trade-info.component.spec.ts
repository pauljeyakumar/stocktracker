/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { StockTrackerTestModule } from '../../../test.module';
import { StockDailyTradeInfoComponent } from '../../../../../../main/webapp/app/entities/stock-daily-trade-info/stock-daily-trade-info.component';
import { StockDailyTradeInfoService } from '../../../../../../main/webapp/app/entities/stock-daily-trade-info/stock-daily-trade-info.service';
import { StockDailyTradeInfo } from '../../../../../../main/webapp/app/entities/stock-daily-trade-info/stock-daily-trade-info.model';

describe('Component Tests', () => {

    describe('StockDailyTradeInfo Management Component', () => {
        let comp: StockDailyTradeInfoComponent;
        let fixture: ComponentFixture<StockDailyTradeInfoComponent>;
        let service: StockDailyTradeInfoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [StockTrackerTestModule],
                declarations: [StockDailyTradeInfoComponent],
                providers: [
                    StockDailyTradeInfoService
                ]
            })
            .overrideTemplate(StockDailyTradeInfoComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(StockDailyTradeInfoComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StockDailyTradeInfoService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new StockDailyTradeInfo(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.stockDailyTradeInfos[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
