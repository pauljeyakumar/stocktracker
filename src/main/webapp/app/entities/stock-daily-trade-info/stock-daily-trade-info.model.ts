import { BaseEntity } from './../../shared';

export class StockDailyTradeInfo implements BaseEntity {
    constructor(
        public id?: number,
        public tradedDate?: any,
        public dayOpen?: number,
        public dayClose?: number,
        public dayHigh?: number,
        public dayLow?: number,
        public change?: number,
        public changePercentage?: number,
        public tradedVolume?: number,
        public tradedValue?: number,
        public deliveryPercentage?: number,
        public yearHigh?: number,
        public yearLow?: number,
        public stock?: BaseEntity,
    ) {
    }
}
