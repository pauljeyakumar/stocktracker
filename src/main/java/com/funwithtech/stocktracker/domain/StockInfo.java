package com.funwithtech.stocktracker.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.util.Objects;

import com.funwithtech.stocktracker.domain.enumeration.Sector;

/**
 * A StockInfo.
 */
@Entity
@Table(name = "stock_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "stockinfo")
public class StockInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "sector", nullable = false)
    private Sector sector;

    @NotNull
    @Column(name = "earning_per_stock", nullable = false)
    private Float earningPerStock;

    @Column(name = "divident")
    private Float divident;

    @NotNull
    @Column(name = "face_value", nullable = false)
    private Integer faceValue;

    @NotNull
    @Column(name = "book_value", nullable = false)
    private Float bookValue;

    @OneToOne
    @JoinColumn(unique = true)
    private Stock stock;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Sector getSector() {
        return sector;
    }

    public StockInfo sector(Sector sector) {
        this.sector = sector;
        return this;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public Float getEarningPerStock() {
        return earningPerStock;
    }

    public StockInfo earningPerStock(Float earningPerStock) {
        this.earningPerStock = earningPerStock;
        return this;
    }

    public void setEarningPerStock(Float earningPerStock) {
        this.earningPerStock = earningPerStock;
    }

    public Float getDivident() {
        return divident;
    }

    public StockInfo divident(Float divident) {
        this.divident = divident;
        return this;
    }

    public void setDivident(Float divident) {
        this.divident = divident;
    }

    public Integer getFaceValue() {
        return faceValue;
    }

    public StockInfo faceValue(Integer faceValue) {
        this.faceValue = faceValue;
        return this;
    }

    public void setFaceValue(Integer faceValue) {
        this.faceValue = faceValue;
    }

    public Float getBookValue() {
        return bookValue;
    }

    public StockInfo bookValue(Float bookValue) {
        this.bookValue = bookValue;
        return this;
    }

    public void setBookValue(Float bookValue) {
        this.bookValue = bookValue;
    }

    public Stock getStock() {
        return stock;
    }

    public StockInfo stock(Stock stock) {
        this.stock = stock;
        return this;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StockInfo stockInfo = (StockInfo) o;
        if (stockInfo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), stockInfo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StockInfo{" +
            "id=" + getId() +
            ", sector='" + getSector() + "'" +
            ", earningPerStock=" + getEarningPerStock() +
            ", divident=" + getDivident() +
            ", faceValue=" + getFaceValue() +
            ", bookValue=" + getBookValue() +
            "}";
    }
}
