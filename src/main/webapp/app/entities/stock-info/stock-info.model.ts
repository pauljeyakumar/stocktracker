import { BaseEntity } from './../../shared';

export const enum Sector {
    'AUTO',
    'BANK',
    'CEMENT',
    'CHEMICALS',
    'CONSTRUCTION',
    'ENGINEERING',
    'FMGC',
    'METALS',
    'POWER',
    'SUGAR',
    'TEXTILES'
}

export class StockInfo implements BaseEntity {
    constructor(
        public id?: number,
        public sector?: Sector,
        public earningPerStock?: number,
        public divident?: number,
        public faceValue?: number,
        public bookValue?: number,
        public stock?: BaseEntity,
    ) {
    }
}
