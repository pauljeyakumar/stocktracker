import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { StockDailyTradeInfoComponent } from './stock-daily-trade-info.component';
import { StockDailyTradeInfoDetailComponent } from './stock-daily-trade-info-detail.component';
import { StockDailyTradeInfoPopupComponent } from './stock-daily-trade-info-dialog.component';
import { StockDailyTradeInfoDeletePopupComponent } from './stock-daily-trade-info-delete-dialog.component';

export const stockDailyTradeInfoRoute: Routes = [
    {
        path: 'stock-daily-trade-info',
        component: StockDailyTradeInfoComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'stockTrackerApp.stockDailyTradeInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'stock-daily-trade-info/:id',
        component: StockDailyTradeInfoDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'stockTrackerApp.stockDailyTradeInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const stockDailyTradeInfoPopupRoute: Routes = [
    {
        path: 'stock-daily-trade-info-new',
        component: StockDailyTradeInfoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'stockTrackerApp.stockDailyTradeInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stock-daily-trade-info/:id/edit',
        component: StockDailyTradeInfoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'stockTrackerApp.stockDailyTradeInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stock-daily-trade-info/:id/delete',
        component: StockDailyTradeInfoDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'stockTrackerApp.stockDailyTradeInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
