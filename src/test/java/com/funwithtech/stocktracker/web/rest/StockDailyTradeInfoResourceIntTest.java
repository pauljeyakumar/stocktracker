package com.funwithtech.stocktracker.web.rest;

import com.funwithtech.stocktracker.StockTrackerApp;

import com.funwithtech.stocktracker.domain.StockDailyTradeInfo;
import com.funwithtech.stocktracker.repository.StockDailyTradeInfoRepository;
import com.funwithtech.stocktracker.repository.search.StockDailyTradeInfoSearchRepository;
import com.funwithtech.stocktracker.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.funwithtech.stocktracker.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the StockDailyTradeInfoResource REST controller.
 *
 * @see StockDailyTradeInfoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StockTrackerApp.class)
public class StockDailyTradeInfoResourceIntTest {

    private static final LocalDate DEFAULT_TRADED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TRADED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Double DEFAULT_DAY_OPEN = 1D;
    private static final Double UPDATED_DAY_OPEN = 2D;

    private static final Double DEFAULT_DAY_CLOSE = 1D;
    private static final Double UPDATED_DAY_CLOSE = 2D;

    private static final Double DEFAULT_DAY_HIGH = 1D;
    private static final Double UPDATED_DAY_HIGH = 2D;

    private static final Double DEFAULT_DAY_LOW = 1D;
    private static final Double UPDATED_DAY_LOW = 2D;

    private static final Float DEFAULT_CHANGE = 1F;
    private static final Float UPDATED_CHANGE = 2F;

    private static final Float DEFAULT_CHANGE_PERCENTAGE = 1F;
    private static final Float UPDATED_CHANGE_PERCENTAGE = 2F;

    private static final Long DEFAULT_TRADED_VOLUME = 1L;
    private static final Long UPDATED_TRADED_VOLUME = 2L;

    private static final Double DEFAULT_TRADED_VALUE = 1D;
    private static final Double UPDATED_TRADED_VALUE = 2D;

    private static final Float DEFAULT_DELIVERY_PERCENTAGE = 1F;
    private static final Float UPDATED_DELIVERY_PERCENTAGE = 2F;

    private static final Double DEFAULT_YEAR_HIGH = 1D;
    private static final Double UPDATED_YEAR_HIGH = 2D;

    private static final Double DEFAULT_YEAR_LOW = 1D;
    private static final Double UPDATED_YEAR_LOW = 2D;

    @Autowired
    private StockDailyTradeInfoRepository stockDailyTradeInfoRepository;

    @Autowired
    private StockDailyTradeInfoSearchRepository stockDailyTradeInfoSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restStockDailyTradeInfoMockMvc;

    private StockDailyTradeInfo stockDailyTradeInfo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final StockDailyTradeInfoResource stockDailyTradeInfoResource = new StockDailyTradeInfoResource(stockDailyTradeInfoRepository, stockDailyTradeInfoSearchRepository);
        this.restStockDailyTradeInfoMockMvc = MockMvcBuilders.standaloneSetup(stockDailyTradeInfoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StockDailyTradeInfo createEntity(EntityManager em) {
        StockDailyTradeInfo stockDailyTradeInfo = new StockDailyTradeInfo()
            .tradedDate(DEFAULT_TRADED_DATE)
            .dayOpen(DEFAULT_DAY_OPEN)
            .dayClose(DEFAULT_DAY_CLOSE)
            .dayHigh(DEFAULT_DAY_HIGH)
            .dayLow(DEFAULT_DAY_LOW)
            .change(DEFAULT_CHANGE)
            .changePercentage(DEFAULT_CHANGE_PERCENTAGE)
            .tradedVolume(DEFAULT_TRADED_VOLUME)
            .tradedValue(DEFAULT_TRADED_VALUE)
            .deliveryPercentage(DEFAULT_DELIVERY_PERCENTAGE)
            .yearHigh(DEFAULT_YEAR_HIGH)
            .yearLow(DEFAULT_YEAR_LOW);
        return stockDailyTradeInfo;
    }

    @Before
    public void initTest() {
        stockDailyTradeInfoSearchRepository.deleteAll();
        stockDailyTradeInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void createStockDailyTradeInfo() throws Exception {
        int databaseSizeBeforeCreate = stockDailyTradeInfoRepository.findAll().size();

        // Create the StockDailyTradeInfo
        restStockDailyTradeInfoMockMvc.perform(post("/api/stock-daily-trade-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stockDailyTradeInfo)))
            .andExpect(status().isCreated());

        // Validate the StockDailyTradeInfo in the database
        List<StockDailyTradeInfo> stockDailyTradeInfoList = stockDailyTradeInfoRepository.findAll();
        assertThat(stockDailyTradeInfoList).hasSize(databaseSizeBeforeCreate + 1);
        StockDailyTradeInfo testStockDailyTradeInfo = stockDailyTradeInfoList.get(stockDailyTradeInfoList.size() - 1);
        assertThat(testStockDailyTradeInfo.getTradedDate()).isEqualTo(DEFAULT_TRADED_DATE);
        assertThat(testStockDailyTradeInfo.getDayOpen()).isEqualTo(DEFAULT_DAY_OPEN);
        assertThat(testStockDailyTradeInfo.getDayClose()).isEqualTo(DEFAULT_DAY_CLOSE);
        assertThat(testStockDailyTradeInfo.getDayHigh()).isEqualTo(DEFAULT_DAY_HIGH);
        assertThat(testStockDailyTradeInfo.getDayLow()).isEqualTo(DEFAULT_DAY_LOW);
        assertThat(testStockDailyTradeInfo.getChange()).isEqualTo(DEFAULT_CHANGE);
        assertThat(testStockDailyTradeInfo.getChangePercentage()).isEqualTo(DEFAULT_CHANGE_PERCENTAGE);
        assertThat(testStockDailyTradeInfo.getTradedVolume()).isEqualTo(DEFAULT_TRADED_VOLUME);
        assertThat(testStockDailyTradeInfo.getTradedValue()).isEqualTo(DEFAULT_TRADED_VALUE);
        assertThat(testStockDailyTradeInfo.getDeliveryPercentage()).isEqualTo(DEFAULT_DELIVERY_PERCENTAGE);
        assertThat(testStockDailyTradeInfo.getYearHigh()).isEqualTo(DEFAULT_YEAR_HIGH);
        assertThat(testStockDailyTradeInfo.getYearLow()).isEqualTo(DEFAULT_YEAR_LOW);

        // Validate the StockDailyTradeInfo in Elasticsearch
        StockDailyTradeInfo stockDailyTradeInfoEs = stockDailyTradeInfoSearchRepository.findOne(testStockDailyTradeInfo.getId());
        assertThat(stockDailyTradeInfoEs).isEqualToIgnoringGivenFields(testStockDailyTradeInfo);
    }

    @Test
    @Transactional
    public void createStockDailyTradeInfoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = stockDailyTradeInfoRepository.findAll().size();

        // Create the StockDailyTradeInfo with an existing ID
        stockDailyTradeInfo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStockDailyTradeInfoMockMvc.perform(post("/api/stock-daily-trade-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stockDailyTradeInfo)))
            .andExpect(status().isBadRequest());

        // Validate the StockDailyTradeInfo in the database
        List<StockDailyTradeInfo> stockDailyTradeInfoList = stockDailyTradeInfoRepository.findAll();
        assertThat(stockDailyTradeInfoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTradedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = stockDailyTradeInfoRepository.findAll().size();
        // set the field null
        stockDailyTradeInfo.setTradedDate(null);

        // Create the StockDailyTradeInfo, which fails.

        restStockDailyTradeInfoMockMvc.perform(post("/api/stock-daily-trade-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stockDailyTradeInfo)))
            .andExpect(status().isBadRequest());

        List<StockDailyTradeInfo> stockDailyTradeInfoList = stockDailyTradeInfoRepository.findAll();
        assertThat(stockDailyTradeInfoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllStockDailyTradeInfos() throws Exception {
        // Initialize the database
        stockDailyTradeInfoRepository.saveAndFlush(stockDailyTradeInfo);

        // Get all the stockDailyTradeInfoList
        restStockDailyTradeInfoMockMvc.perform(get("/api/stock-daily-trade-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(stockDailyTradeInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].tradedDate").value(hasItem(DEFAULT_TRADED_DATE.toString())))
            .andExpect(jsonPath("$.[*].dayOpen").value(hasItem(DEFAULT_DAY_OPEN.doubleValue())))
            .andExpect(jsonPath("$.[*].dayClose").value(hasItem(DEFAULT_DAY_CLOSE.doubleValue())))
            .andExpect(jsonPath("$.[*].dayHigh").value(hasItem(DEFAULT_DAY_HIGH.doubleValue())))
            .andExpect(jsonPath("$.[*].dayLow").value(hasItem(DEFAULT_DAY_LOW.doubleValue())))
            .andExpect(jsonPath("$.[*].change").value(hasItem(DEFAULT_CHANGE.doubleValue())))
            .andExpect(jsonPath("$.[*].changePercentage").value(hasItem(DEFAULT_CHANGE_PERCENTAGE.doubleValue())))
            .andExpect(jsonPath("$.[*].tradedVolume").value(hasItem(DEFAULT_TRADED_VOLUME.intValue())))
            .andExpect(jsonPath("$.[*].tradedValue").value(hasItem(DEFAULT_TRADED_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].deliveryPercentage").value(hasItem(DEFAULT_DELIVERY_PERCENTAGE.doubleValue())))
            .andExpect(jsonPath("$.[*].yearHigh").value(hasItem(DEFAULT_YEAR_HIGH.doubleValue())))
            .andExpect(jsonPath("$.[*].yearLow").value(hasItem(DEFAULT_YEAR_LOW.doubleValue())));
    }

    @Test
    @Transactional
    public void getStockDailyTradeInfo() throws Exception {
        // Initialize the database
        stockDailyTradeInfoRepository.saveAndFlush(stockDailyTradeInfo);

        // Get the stockDailyTradeInfo
        restStockDailyTradeInfoMockMvc.perform(get("/api/stock-daily-trade-infos/{id}", stockDailyTradeInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(stockDailyTradeInfo.getId().intValue()))
            .andExpect(jsonPath("$.tradedDate").value(DEFAULT_TRADED_DATE.toString()))
            .andExpect(jsonPath("$.dayOpen").value(DEFAULT_DAY_OPEN.doubleValue()))
            .andExpect(jsonPath("$.dayClose").value(DEFAULT_DAY_CLOSE.doubleValue()))
            .andExpect(jsonPath("$.dayHigh").value(DEFAULT_DAY_HIGH.doubleValue()))
            .andExpect(jsonPath("$.dayLow").value(DEFAULT_DAY_LOW.doubleValue()))
            .andExpect(jsonPath("$.change").value(DEFAULT_CHANGE.doubleValue()))
            .andExpect(jsonPath("$.changePercentage").value(DEFAULT_CHANGE_PERCENTAGE.doubleValue()))
            .andExpect(jsonPath("$.tradedVolume").value(DEFAULT_TRADED_VOLUME.intValue()))
            .andExpect(jsonPath("$.tradedValue").value(DEFAULT_TRADED_VALUE.doubleValue()))
            .andExpect(jsonPath("$.deliveryPercentage").value(DEFAULT_DELIVERY_PERCENTAGE.doubleValue()))
            .andExpect(jsonPath("$.yearHigh").value(DEFAULT_YEAR_HIGH.doubleValue()))
            .andExpect(jsonPath("$.yearLow").value(DEFAULT_YEAR_LOW.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingStockDailyTradeInfo() throws Exception {
        // Get the stockDailyTradeInfo
        restStockDailyTradeInfoMockMvc.perform(get("/api/stock-daily-trade-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStockDailyTradeInfo() throws Exception {
        // Initialize the database
        stockDailyTradeInfoRepository.saveAndFlush(stockDailyTradeInfo);
        stockDailyTradeInfoSearchRepository.save(stockDailyTradeInfo);
        int databaseSizeBeforeUpdate = stockDailyTradeInfoRepository.findAll().size();

        // Update the stockDailyTradeInfo
        StockDailyTradeInfo updatedStockDailyTradeInfo = stockDailyTradeInfoRepository.findOne(stockDailyTradeInfo.getId());
        // Disconnect from session so that the updates on updatedStockDailyTradeInfo are not directly saved in db
        em.detach(updatedStockDailyTradeInfo);
        updatedStockDailyTradeInfo
            .tradedDate(UPDATED_TRADED_DATE)
            .dayOpen(UPDATED_DAY_OPEN)
            .dayClose(UPDATED_DAY_CLOSE)
            .dayHigh(UPDATED_DAY_HIGH)
            .dayLow(UPDATED_DAY_LOW)
            .change(UPDATED_CHANGE)
            .changePercentage(UPDATED_CHANGE_PERCENTAGE)
            .tradedVolume(UPDATED_TRADED_VOLUME)
            .tradedValue(UPDATED_TRADED_VALUE)
            .deliveryPercentage(UPDATED_DELIVERY_PERCENTAGE)
            .yearHigh(UPDATED_YEAR_HIGH)
            .yearLow(UPDATED_YEAR_LOW);

        restStockDailyTradeInfoMockMvc.perform(put("/api/stock-daily-trade-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedStockDailyTradeInfo)))
            .andExpect(status().isOk());

        // Validate the StockDailyTradeInfo in the database
        List<StockDailyTradeInfo> stockDailyTradeInfoList = stockDailyTradeInfoRepository.findAll();
        assertThat(stockDailyTradeInfoList).hasSize(databaseSizeBeforeUpdate);
        StockDailyTradeInfo testStockDailyTradeInfo = stockDailyTradeInfoList.get(stockDailyTradeInfoList.size() - 1);
        assertThat(testStockDailyTradeInfo.getTradedDate()).isEqualTo(UPDATED_TRADED_DATE);
        assertThat(testStockDailyTradeInfo.getDayOpen()).isEqualTo(UPDATED_DAY_OPEN);
        assertThat(testStockDailyTradeInfo.getDayClose()).isEqualTo(UPDATED_DAY_CLOSE);
        assertThat(testStockDailyTradeInfo.getDayHigh()).isEqualTo(UPDATED_DAY_HIGH);
        assertThat(testStockDailyTradeInfo.getDayLow()).isEqualTo(UPDATED_DAY_LOW);
        assertThat(testStockDailyTradeInfo.getChange()).isEqualTo(UPDATED_CHANGE);
        assertThat(testStockDailyTradeInfo.getChangePercentage()).isEqualTo(UPDATED_CHANGE_PERCENTAGE);
        assertThat(testStockDailyTradeInfo.getTradedVolume()).isEqualTo(UPDATED_TRADED_VOLUME);
        assertThat(testStockDailyTradeInfo.getTradedValue()).isEqualTo(UPDATED_TRADED_VALUE);
        assertThat(testStockDailyTradeInfo.getDeliveryPercentage()).isEqualTo(UPDATED_DELIVERY_PERCENTAGE);
        assertThat(testStockDailyTradeInfo.getYearHigh()).isEqualTo(UPDATED_YEAR_HIGH);
        assertThat(testStockDailyTradeInfo.getYearLow()).isEqualTo(UPDATED_YEAR_LOW);

        // Validate the StockDailyTradeInfo in Elasticsearch
        StockDailyTradeInfo stockDailyTradeInfoEs = stockDailyTradeInfoSearchRepository.findOne(testStockDailyTradeInfo.getId());
        assertThat(stockDailyTradeInfoEs).isEqualToIgnoringGivenFields(testStockDailyTradeInfo);
    }

    @Test
    @Transactional
    public void updateNonExistingStockDailyTradeInfo() throws Exception {
        int databaseSizeBeforeUpdate = stockDailyTradeInfoRepository.findAll().size();

        // Create the StockDailyTradeInfo

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restStockDailyTradeInfoMockMvc.perform(put("/api/stock-daily-trade-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stockDailyTradeInfo)))
            .andExpect(status().isCreated());

        // Validate the StockDailyTradeInfo in the database
        List<StockDailyTradeInfo> stockDailyTradeInfoList = stockDailyTradeInfoRepository.findAll();
        assertThat(stockDailyTradeInfoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteStockDailyTradeInfo() throws Exception {
        // Initialize the database
        stockDailyTradeInfoRepository.saveAndFlush(stockDailyTradeInfo);
        stockDailyTradeInfoSearchRepository.save(stockDailyTradeInfo);
        int databaseSizeBeforeDelete = stockDailyTradeInfoRepository.findAll().size();

        // Get the stockDailyTradeInfo
        restStockDailyTradeInfoMockMvc.perform(delete("/api/stock-daily-trade-infos/{id}", stockDailyTradeInfo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean stockDailyTradeInfoExistsInEs = stockDailyTradeInfoSearchRepository.exists(stockDailyTradeInfo.getId());
        assertThat(stockDailyTradeInfoExistsInEs).isFalse();

        // Validate the database is empty
        List<StockDailyTradeInfo> stockDailyTradeInfoList = stockDailyTradeInfoRepository.findAll();
        assertThat(stockDailyTradeInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchStockDailyTradeInfo() throws Exception {
        // Initialize the database
        stockDailyTradeInfoRepository.saveAndFlush(stockDailyTradeInfo);
        stockDailyTradeInfoSearchRepository.save(stockDailyTradeInfo);

        // Search the stockDailyTradeInfo
        restStockDailyTradeInfoMockMvc.perform(get("/api/_search/stock-daily-trade-infos?query=id:" + stockDailyTradeInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(stockDailyTradeInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].tradedDate").value(hasItem(DEFAULT_TRADED_DATE.toString())))
            .andExpect(jsonPath("$.[*].dayOpen").value(hasItem(DEFAULT_DAY_OPEN.doubleValue())))
            .andExpect(jsonPath("$.[*].dayClose").value(hasItem(DEFAULT_DAY_CLOSE.doubleValue())))
            .andExpect(jsonPath("$.[*].dayHigh").value(hasItem(DEFAULT_DAY_HIGH.doubleValue())))
            .andExpect(jsonPath("$.[*].dayLow").value(hasItem(DEFAULT_DAY_LOW.doubleValue())))
            .andExpect(jsonPath("$.[*].change").value(hasItem(DEFAULT_CHANGE.doubleValue())))
            .andExpect(jsonPath("$.[*].changePercentage").value(hasItem(DEFAULT_CHANGE_PERCENTAGE.doubleValue())))
            .andExpect(jsonPath("$.[*].tradedVolume").value(hasItem(DEFAULT_TRADED_VOLUME.intValue())))
            .andExpect(jsonPath("$.[*].tradedValue").value(hasItem(DEFAULT_TRADED_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].deliveryPercentage").value(hasItem(DEFAULT_DELIVERY_PERCENTAGE.doubleValue())))
            .andExpect(jsonPath("$.[*].yearHigh").value(hasItem(DEFAULT_YEAR_HIGH.doubleValue())))
            .andExpect(jsonPath("$.[*].yearLow").value(hasItem(DEFAULT_YEAR_LOW.doubleValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(StockDailyTradeInfo.class);
        StockDailyTradeInfo stockDailyTradeInfo1 = new StockDailyTradeInfo();
        stockDailyTradeInfo1.setId(1L);
        StockDailyTradeInfo stockDailyTradeInfo2 = new StockDailyTradeInfo();
        stockDailyTradeInfo2.setId(stockDailyTradeInfo1.getId());
        assertThat(stockDailyTradeInfo1).isEqualTo(stockDailyTradeInfo2);
        stockDailyTradeInfo2.setId(2L);
        assertThat(stockDailyTradeInfo1).isNotEqualTo(stockDailyTradeInfo2);
        stockDailyTradeInfo1.setId(null);
        assertThat(stockDailyTradeInfo1).isNotEqualTo(stockDailyTradeInfo2);
    }
}
