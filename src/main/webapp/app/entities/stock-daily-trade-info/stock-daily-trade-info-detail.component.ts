import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { StockDailyTradeInfo } from './stock-daily-trade-info.model';
import { StockDailyTradeInfoService } from './stock-daily-trade-info.service';

@Component({
    selector: 'jhi-stock-daily-trade-info-detail',
    templateUrl: './stock-daily-trade-info-detail.component.html'
})
export class StockDailyTradeInfoDetailComponent implements OnInit, OnDestroy {

    stockDailyTradeInfo: StockDailyTradeInfo;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private stockDailyTradeInfoService: StockDailyTradeInfoService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInStockDailyTradeInfos();
    }

    load(id) {
        this.stockDailyTradeInfoService.find(id)
            .subscribe((stockDailyTradeInfoResponse: HttpResponse<StockDailyTradeInfo>) => {
                this.stockDailyTradeInfo = stockDailyTradeInfoResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInStockDailyTradeInfos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'stockDailyTradeInfoListModification',
            (response) => this.load(this.stockDailyTradeInfo.id)
        );
    }
}
