/**
 * View Models used by Spring MVC REST controllers.
 */
package com.funwithtech.stocktracker.web.rest.vm;
