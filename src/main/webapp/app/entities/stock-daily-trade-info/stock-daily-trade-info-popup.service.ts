import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { StockDailyTradeInfo } from './stock-daily-trade-info.model';
import { StockDailyTradeInfoService } from './stock-daily-trade-info.service';

@Injectable()
export class StockDailyTradeInfoPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private stockDailyTradeInfoService: StockDailyTradeInfoService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.stockDailyTradeInfoService.find(id)
                    .subscribe((stockDailyTradeInfoResponse: HttpResponse<StockDailyTradeInfo>) => {
                        const stockDailyTradeInfo: StockDailyTradeInfo = stockDailyTradeInfoResponse.body;
                        if (stockDailyTradeInfo.tradedDate) {
                            stockDailyTradeInfo.tradedDate = {
                                year: stockDailyTradeInfo.tradedDate.getFullYear(),
                                month: stockDailyTradeInfo.tradedDate.getMonth() + 1,
                                day: stockDailyTradeInfo.tradedDate.getDate()
                            };
                        }
                        this.ngbModalRef = this.stockDailyTradeInfoModalRef(component, stockDailyTradeInfo);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.stockDailyTradeInfoModalRef(component, new StockDailyTradeInfo());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    stockDailyTradeInfoModalRef(component: Component, stockDailyTradeInfo: StockDailyTradeInfo): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.stockDailyTradeInfo = stockDailyTradeInfo;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
