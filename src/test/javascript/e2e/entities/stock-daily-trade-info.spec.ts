import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('StockDailyTradeInfo e2e test', () => {

    let navBarPage: NavBarPage;
    let stockDailyTradeInfoDialogPage: StockDailyTradeInfoDialogPage;
    let stockDailyTradeInfoComponentsPage: StockDailyTradeInfoComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load StockDailyTradeInfos', () => {
        navBarPage.goToEntity('stock-daily-trade-info');
        stockDailyTradeInfoComponentsPage = new StockDailyTradeInfoComponentsPage();
        expect(stockDailyTradeInfoComponentsPage.getTitle())
            .toMatch(/stockTrackerApp.stockDailyTradeInfo.home.title/);

    });

    it('should load create StockDailyTradeInfo dialog', () => {
        stockDailyTradeInfoComponentsPage.clickOnCreateButton();
        stockDailyTradeInfoDialogPage = new StockDailyTradeInfoDialogPage();
        expect(stockDailyTradeInfoDialogPage.getModalTitle())
            .toMatch(/stockTrackerApp.stockDailyTradeInfo.home.createOrEditLabel/);
        stockDailyTradeInfoDialogPage.close();
    });

    it('should create and save StockDailyTradeInfos', () => {
        stockDailyTradeInfoComponentsPage.clickOnCreateButton();
        stockDailyTradeInfoDialogPage.setTradedDateInput('2000-12-31');
        expect(stockDailyTradeInfoDialogPage.getTradedDateInput()).toMatch('2000-12-31');
        stockDailyTradeInfoDialogPage.setDayOpenInput('5');
        expect(stockDailyTradeInfoDialogPage.getDayOpenInput()).toMatch('5');
        stockDailyTradeInfoDialogPage.setDayCloseInput('5');
        expect(stockDailyTradeInfoDialogPage.getDayCloseInput()).toMatch('5');
        stockDailyTradeInfoDialogPage.setDayHighInput('5');
        expect(stockDailyTradeInfoDialogPage.getDayHighInput()).toMatch('5');
        stockDailyTradeInfoDialogPage.setDayLowInput('5');
        expect(stockDailyTradeInfoDialogPage.getDayLowInput()).toMatch('5');
        stockDailyTradeInfoDialogPage.setChangeInput('5');
        expect(stockDailyTradeInfoDialogPage.getChangeInput()).toMatch('5');
        stockDailyTradeInfoDialogPage.setChangePercentageInput('5');
        expect(stockDailyTradeInfoDialogPage.getChangePercentageInput()).toMatch('5');
        stockDailyTradeInfoDialogPage.setTradedVolumeInput('5');
        expect(stockDailyTradeInfoDialogPage.getTradedVolumeInput()).toMatch('5');
        stockDailyTradeInfoDialogPage.setTradedValueInput('5');
        expect(stockDailyTradeInfoDialogPage.getTradedValueInput()).toMatch('5');
        stockDailyTradeInfoDialogPage.setDeliveryPercentageInput('5');
        expect(stockDailyTradeInfoDialogPage.getDeliveryPercentageInput()).toMatch('5');
        stockDailyTradeInfoDialogPage.setYearHighInput('5');
        expect(stockDailyTradeInfoDialogPage.getYearHighInput()).toMatch('5');
        stockDailyTradeInfoDialogPage.setYearLowInput('5');
        expect(stockDailyTradeInfoDialogPage.getYearLowInput()).toMatch('5');
        stockDailyTradeInfoDialogPage.stockSelectLastOption();
        stockDailyTradeInfoDialogPage.save();
        expect(stockDailyTradeInfoDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class StockDailyTradeInfoComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-stock-daily-trade-info div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class StockDailyTradeInfoDialogPage {
    modalTitle = element(by.css('h4#myStockDailyTradeInfoLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    tradedDateInput = element(by.css('input#field_tradedDate'));
    dayOpenInput = element(by.css('input#field_dayOpen'));
    dayCloseInput = element(by.css('input#field_dayClose'));
    dayHighInput = element(by.css('input#field_dayHigh'));
    dayLowInput = element(by.css('input#field_dayLow'));
    changeInput = element(by.css('input#field_change'));
    changePercentageInput = element(by.css('input#field_changePercentage'));
    tradedVolumeInput = element(by.css('input#field_tradedVolume'));
    tradedValueInput = element(by.css('input#field_tradedValue'));
    deliveryPercentageInput = element(by.css('input#field_deliveryPercentage'));
    yearHighInput = element(by.css('input#field_yearHigh'));
    yearLowInput = element(by.css('input#field_yearLow'));
    stockSelect = element(by.css('select#field_stock'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setTradedDateInput = function(tradedDate) {
        this.tradedDateInput.sendKeys(tradedDate);
    };

    getTradedDateInput = function() {
        return this.tradedDateInput.getAttribute('value');
    };

    setDayOpenInput = function(dayOpen) {
        this.dayOpenInput.sendKeys(dayOpen);
    };

    getDayOpenInput = function() {
        return this.dayOpenInput.getAttribute('value');
    };

    setDayCloseInput = function(dayClose) {
        this.dayCloseInput.sendKeys(dayClose);
    };

    getDayCloseInput = function() {
        return this.dayCloseInput.getAttribute('value');
    };

    setDayHighInput = function(dayHigh) {
        this.dayHighInput.sendKeys(dayHigh);
    };

    getDayHighInput = function() {
        return this.dayHighInput.getAttribute('value');
    };

    setDayLowInput = function(dayLow) {
        this.dayLowInput.sendKeys(dayLow);
    };

    getDayLowInput = function() {
        return this.dayLowInput.getAttribute('value');
    };

    setChangeInput = function(change) {
        this.changeInput.sendKeys(change);
    };

    getChangeInput = function() {
        return this.changeInput.getAttribute('value');
    };

    setChangePercentageInput = function(changePercentage) {
        this.changePercentageInput.sendKeys(changePercentage);
    };

    getChangePercentageInput = function() {
        return this.changePercentageInput.getAttribute('value');
    };

    setTradedVolumeInput = function(tradedVolume) {
        this.tradedVolumeInput.sendKeys(tradedVolume);
    };

    getTradedVolumeInput = function() {
        return this.tradedVolumeInput.getAttribute('value');
    };

    setTradedValueInput = function(tradedValue) {
        this.tradedValueInput.sendKeys(tradedValue);
    };

    getTradedValueInput = function() {
        return this.tradedValueInput.getAttribute('value');
    };

    setDeliveryPercentageInput = function(deliveryPercentage) {
        this.deliveryPercentageInput.sendKeys(deliveryPercentage);
    };

    getDeliveryPercentageInput = function() {
        return this.deliveryPercentageInput.getAttribute('value');
    };

    setYearHighInput = function(yearHigh) {
        this.yearHighInput.sendKeys(yearHigh);
    };

    getYearHighInput = function() {
        return this.yearHighInput.getAttribute('value');
    };

    setYearLowInput = function(yearLow) {
        this.yearLowInput.sendKeys(yearLow);
    };

    getYearLowInput = function() {
        return this.yearLowInput.getAttribute('value');
    };

    stockSelectLastOption = function() {
        this.stockSelect.all(by.tagName('option')).last().click();
    };

    stockSelectOption = function(option) {
        this.stockSelect.sendKeys(option);
    };

    getStockSelect = function() {
        return this.stockSelect;
    };

    getStockSelectedOption = function() {
        return this.stockSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
