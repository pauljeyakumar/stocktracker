package com.funwithtech.stocktracker.cucumber.stepdefs;

import com.funwithtech.stocktracker.StockTrackerApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = StockTrackerApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
