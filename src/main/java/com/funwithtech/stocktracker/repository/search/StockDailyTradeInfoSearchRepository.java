package com.funwithtech.stocktracker.repository.search;

import com.funwithtech.stocktracker.domain.StockDailyTradeInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the StockDailyTradeInfo entity.
 */
public interface StockDailyTradeInfoSearchRepository extends ElasticsearchRepository<StockDailyTradeInfo, Long> {
}
