/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { StockTrackerTestModule } from '../../../test.module';
import { StockInfoComponent } from '../../../../../../main/webapp/app/entities/stock-info/stock-info.component';
import { StockInfoService } from '../../../../../../main/webapp/app/entities/stock-info/stock-info.service';
import { StockInfo } from '../../../../../../main/webapp/app/entities/stock-info/stock-info.model';

describe('Component Tests', () => {

    describe('StockInfo Management Component', () => {
        let comp: StockInfoComponent;
        let fixture: ComponentFixture<StockInfoComponent>;
        let service: StockInfoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [StockTrackerTestModule],
                declarations: [StockInfoComponent],
                providers: [
                    StockInfoService
                ]
            })
            .overrideTemplate(StockInfoComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(StockInfoComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StockInfoService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new StockInfo(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.stockInfos[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
