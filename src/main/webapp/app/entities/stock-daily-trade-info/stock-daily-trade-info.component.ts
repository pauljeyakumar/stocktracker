import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { StockDailyTradeInfo } from './stock-daily-trade-info.model';
import { StockDailyTradeInfoService } from './stock-daily-trade-info.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-stock-daily-trade-info',
    templateUrl: './stock-daily-trade-info.component.html'
})
export class StockDailyTradeInfoComponent implements OnInit, OnDestroy {
stockDailyTradeInfos: StockDailyTradeInfo[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private stockDailyTradeInfoService: StockDailyTradeInfoService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.stockDailyTradeInfoService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<StockDailyTradeInfo[]>) => this.stockDailyTradeInfos = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.stockDailyTradeInfoService.query().subscribe(
            (res: HttpResponse<StockDailyTradeInfo[]>) => {
                this.stockDailyTradeInfos = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInStockDailyTradeInfos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: StockDailyTradeInfo) {
        return item.id;
    }
    registerChangeInStockDailyTradeInfos() {
        this.eventSubscriber = this.eventManager.subscribe('stockDailyTradeInfoListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
