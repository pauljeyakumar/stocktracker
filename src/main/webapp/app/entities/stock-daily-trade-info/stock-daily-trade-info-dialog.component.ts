import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { StockDailyTradeInfo } from './stock-daily-trade-info.model';
import { StockDailyTradeInfoPopupService } from './stock-daily-trade-info-popup.service';
import { StockDailyTradeInfoService } from './stock-daily-trade-info.service';
import { Stock, StockService } from '../stock';

@Component({
    selector: 'jhi-stock-daily-trade-info-dialog',
    templateUrl: './stock-daily-trade-info-dialog.component.html'
})
export class StockDailyTradeInfoDialogComponent implements OnInit {

    stockDailyTradeInfo: StockDailyTradeInfo;
    isSaving: boolean;

    stocks: Stock[];
    tradedDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private stockDailyTradeInfoService: StockDailyTradeInfoService,
        private stockService: StockService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.stockService.query()
            .subscribe((res: HttpResponse<Stock[]>) => { this.stocks = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.stockDailyTradeInfo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.stockDailyTradeInfoService.update(this.stockDailyTradeInfo));
        } else {
            this.subscribeToSaveResponse(
                this.stockDailyTradeInfoService.create(this.stockDailyTradeInfo));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<StockDailyTradeInfo>>) {
        result.subscribe((res: HttpResponse<StockDailyTradeInfo>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: StockDailyTradeInfo) {
        this.eventManager.broadcast({ name: 'stockDailyTradeInfoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackStockById(index: number, item: Stock) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-stock-daily-trade-info-popup',
    template: ''
})
export class StockDailyTradeInfoPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private stockDailyTradeInfoPopupService: StockDailyTradeInfoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.stockDailyTradeInfoPopupService
                    .open(StockDailyTradeInfoDialogComponent as Component, params['id']);
            } else {
                this.stockDailyTradeInfoPopupService
                    .open(StockDailyTradeInfoDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
