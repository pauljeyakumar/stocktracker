package com.funwithtech.stocktracker.domain.enumeration;

/**
 * The Exchange enumeration.
 */
public enum Exchange {
    NSE, BSE
}
