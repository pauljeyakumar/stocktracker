import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { StockInfo } from './stock-info.model';
import { StockInfoService } from './stock-info.service';

@Component({
    selector: 'jhi-stock-info-detail',
    templateUrl: './stock-info-detail.component.html'
})
export class StockInfoDetailComponent implements OnInit, OnDestroy {

    stockInfo: StockInfo;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private stockInfoService: StockInfoService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInStockInfos();
    }

    load(id) {
        this.stockInfoService.find(id)
            .subscribe((stockInfoResponse: HttpResponse<StockInfo>) => {
                this.stockInfo = stockInfoResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInStockInfos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'stockInfoListModification',
            (response) => this.load(this.stockInfo.id)
        );
    }
}
