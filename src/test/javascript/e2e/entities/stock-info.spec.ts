import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('StockInfo e2e test', () => {

    let navBarPage: NavBarPage;
    let stockInfoDialogPage: StockInfoDialogPage;
    let stockInfoComponentsPage: StockInfoComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load StockInfos', () => {
        navBarPage.goToEntity('stock-info');
        stockInfoComponentsPage = new StockInfoComponentsPage();
        expect(stockInfoComponentsPage.getTitle())
            .toMatch(/stockTrackerApp.stockInfo.home.title/);

    });

    it('should load create StockInfo dialog', () => {
        stockInfoComponentsPage.clickOnCreateButton();
        stockInfoDialogPage = new StockInfoDialogPage();
        expect(stockInfoDialogPage.getModalTitle())
            .toMatch(/stockTrackerApp.stockInfo.home.createOrEditLabel/);
        stockInfoDialogPage.close();
    });

    it('should create and save StockInfos', () => {
        stockInfoComponentsPage.clickOnCreateButton();
        stockInfoDialogPage.sectorSelectLastOption();
        stockInfoDialogPage.setEarningPerStockInput('5');
        expect(stockInfoDialogPage.getEarningPerStockInput()).toMatch('5');
        stockInfoDialogPage.setDividentInput('5');
        expect(stockInfoDialogPage.getDividentInput()).toMatch('5');
        stockInfoDialogPage.setFaceValueInput('5');
        expect(stockInfoDialogPage.getFaceValueInput()).toMatch('5');
        stockInfoDialogPage.setBookValueInput('5');
        expect(stockInfoDialogPage.getBookValueInput()).toMatch('5');
        stockInfoDialogPage.stockSelectLastOption();
        stockInfoDialogPage.save();
        expect(stockInfoDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class StockInfoComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-stock-info div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class StockInfoDialogPage {
    modalTitle = element(by.css('h4#myStockInfoLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    sectorSelect = element(by.css('select#field_sector'));
    earningPerStockInput = element(by.css('input#field_earningPerStock'));
    dividentInput = element(by.css('input#field_divident'));
    faceValueInput = element(by.css('input#field_faceValue'));
    bookValueInput = element(by.css('input#field_bookValue'));
    stockSelect = element(by.css('select#field_stock'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setSectorSelect = function(sector) {
        this.sectorSelect.sendKeys(sector);
    };

    getSectorSelect = function() {
        return this.sectorSelect.element(by.css('option:checked')).getText();
    };

    sectorSelectLastOption = function() {
        this.sectorSelect.all(by.tagName('option')).last().click();
    };
    setEarningPerStockInput = function(earningPerStock) {
        this.earningPerStockInput.sendKeys(earningPerStock);
    };

    getEarningPerStockInput = function() {
        return this.earningPerStockInput.getAttribute('value');
    };

    setDividentInput = function(divident) {
        this.dividentInput.sendKeys(divident);
    };

    getDividentInput = function() {
        return this.dividentInput.getAttribute('value');
    };

    setFaceValueInput = function(faceValue) {
        this.faceValueInput.sendKeys(faceValue);
    };

    getFaceValueInput = function() {
        return this.faceValueInput.getAttribute('value');
    };

    setBookValueInput = function(bookValue) {
        this.bookValueInput.sendKeys(bookValue);
    };

    getBookValueInput = function() {
        return this.bookValueInput.getAttribute('value');
    };

    stockSelectLastOption = function() {
        this.stockSelect.all(by.tagName('option')).last().click();
    };

    stockSelectOption = function(option) {
        this.stockSelect.sendKeys(option);
    };

    getStockSelect = function() {
        return this.stockSelect;
    };

    getStockSelectedOption = function() {
        return this.stockSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
