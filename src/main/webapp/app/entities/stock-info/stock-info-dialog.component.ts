import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { StockInfo } from './stock-info.model';
import { StockInfoPopupService } from './stock-info-popup.service';
import { StockInfoService } from './stock-info.service';
import { Stock, StockService } from '../stock';

@Component({
    selector: 'jhi-stock-info-dialog',
    templateUrl: './stock-info-dialog.component.html'
})
export class StockInfoDialogComponent implements OnInit {

    stockInfo: StockInfo;
    isSaving: boolean;

    stocks: Stock[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private stockInfoService: StockInfoService,
        private stockService: StockService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.stockService
            .query({filter: 'stockinfo-is-null'})
            .subscribe((res: HttpResponse<Stock[]>) => {
                if (!this.stockInfo.stock || !this.stockInfo.stock.id) {
                    this.stocks = res.body;
                } else {
                    this.stockService
                        .find(this.stockInfo.stock.id)
                        .subscribe((subRes: HttpResponse<Stock>) => {
                            this.stocks = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.stockInfo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.stockInfoService.update(this.stockInfo));
        } else {
            this.subscribeToSaveResponse(
                this.stockInfoService.create(this.stockInfo));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<StockInfo>>) {
        result.subscribe((res: HttpResponse<StockInfo>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: StockInfo) {
        this.eventManager.broadcast({ name: 'stockInfoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackStockById(index: number, item: Stock) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-stock-info-popup',
    template: ''
})
export class StockInfoPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private stockInfoPopupService: StockInfoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.stockInfoPopupService
                    .open(StockInfoDialogComponent as Component, params['id']);
            } else {
                this.stockInfoPopupService
                    .open(StockInfoDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
