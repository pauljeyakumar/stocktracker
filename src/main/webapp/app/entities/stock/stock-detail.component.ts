import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Stock } from './stock.model';
import { StockService } from './stock.service';

@Component({
    selector: 'jhi-stock-detail',
    templateUrl: './stock-detail.component.html'
})
export class StockDetailComponent implements OnInit, OnDestroy {

    stock: Stock;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private stockService: StockService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInStocks();
    }

    load(id) {
        this.stockService.find(id)
            .subscribe((stockResponse: HttpResponse<Stock>) => {
                this.stock = stockResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInStocks() {
        this.eventSubscriber = this.eventManager.subscribe(
            'stockListModification',
            (response) => this.load(this.stock.id)
        );
    }
}
