package com.funwithtech.stocktracker.domain.enumeration;

/**
 * The Sector enumeration.
 */
public enum Sector {
    AUTO, BANK, CEMENT, CHEMICALS, CONSTRUCTION, ENGINEERING, FMGC, METALS, POWER, SUGAR, TEXTILES
}
