package com.funwithtech.stocktracker.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A StockDailyTradeInfo.
 */
@Entity
@Table(name = "stock_daily_trade_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "stockdailytradeinfo")
public class StockDailyTradeInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "traded_date", nullable = false)
    private LocalDate tradedDate;

    @Column(name = "day_open")
    private Double dayOpen;

    @Column(name = "day_close")
    private Double dayClose;

    @Column(name = "day_high")
    private Double dayHigh;

    @Column(name = "day_low")
    private Double dayLow;

    @Column(name = "jhi_change")
    private Float change;

    @Column(name = "change_percentage")
    private Float changePercentage;

    @Column(name = "traded_volume")
    private Long tradedVolume;

    @Column(name = "traded_value")
    private Double tradedValue;

    @Column(name = "delivery_percentage")
    private Float deliveryPercentage;

    @Column(name = "year_high")
    private Double yearHigh;

    @Column(name = "year_low")
    private Double yearLow;

    @ManyToOne
    private Stock stock;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTradedDate() {
        return tradedDate;
    }

    public StockDailyTradeInfo tradedDate(LocalDate tradedDate) {
        this.tradedDate = tradedDate;
        return this;
    }

    public void setTradedDate(LocalDate tradedDate) {
        this.tradedDate = tradedDate;
    }

    public Double getDayOpen() {
        return dayOpen;
    }

    public StockDailyTradeInfo dayOpen(Double dayOpen) {
        this.dayOpen = dayOpen;
        return this;
    }

    public void setDayOpen(Double dayOpen) {
        this.dayOpen = dayOpen;
    }

    public Double getDayClose() {
        return dayClose;
    }

    public StockDailyTradeInfo dayClose(Double dayClose) {
        this.dayClose = dayClose;
        return this;
    }

    public void setDayClose(Double dayClose) {
        this.dayClose = dayClose;
    }

    public Double getDayHigh() {
        return dayHigh;
    }

    public StockDailyTradeInfo dayHigh(Double dayHigh) {
        this.dayHigh = dayHigh;
        return this;
    }

    public void setDayHigh(Double dayHigh) {
        this.dayHigh = dayHigh;
    }

    public Double getDayLow() {
        return dayLow;
    }

    public StockDailyTradeInfo dayLow(Double dayLow) {
        this.dayLow = dayLow;
        return this;
    }

    public void setDayLow(Double dayLow) {
        this.dayLow = dayLow;
    }

    public Float getChange() {
        return change;
    }

    public StockDailyTradeInfo change(Float change) {
        this.change = change;
        return this;
    }

    public void setChange(Float change) {
        this.change = change;
    }

    public Float getChangePercentage() {
        return changePercentage;
    }

    public StockDailyTradeInfo changePercentage(Float changePercentage) {
        this.changePercentage = changePercentage;
        return this;
    }

    public void setChangePercentage(Float changePercentage) {
        this.changePercentage = changePercentage;
    }

    public Long getTradedVolume() {
        return tradedVolume;
    }

    public StockDailyTradeInfo tradedVolume(Long tradedVolume) {
        this.tradedVolume = tradedVolume;
        return this;
    }

    public void setTradedVolume(Long tradedVolume) {
        this.tradedVolume = tradedVolume;
    }

    public Double getTradedValue() {
        return tradedValue;
    }

    public StockDailyTradeInfo tradedValue(Double tradedValue) {
        this.tradedValue = tradedValue;
        return this;
    }

    public void setTradedValue(Double tradedValue) {
        this.tradedValue = tradedValue;
    }

    public Float getDeliveryPercentage() {
        return deliveryPercentage;
    }

    public StockDailyTradeInfo deliveryPercentage(Float deliveryPercentage) {
        this.deliveryPercentage = deliveryPercentage;
        return this;
    }

    public void setDeliveryPercentage(Float deliveryPercentage) {
        this.deliveryPercentage = deliveryPercentage;
    }

    public Double getYearHigh() {
        return yearHigh;
    }

    public StockDailyTradeInfo yearHigh(Double yearHigh) {
        this.yearHigh = yearHigh;
        return this;
    }

    public void setYearHigh(Double yearHigh) {
        this.yearHigh = yearHigh;
    }

    public Double getYearLow() {
        return yearLow;
    }

    public StockDailyTradeInfo yearLow(Double yearLow) {
        this.yearLow = yearLow;
        return this;
    }

    public void setYearLow(Double yearLow) {
        this.yearLow = yearLow;
    }

    public Stock getStock() {
        return stock;
    }

    public StockDailyTradeInfo stock(Stock stock) {
        this.stock = stock;
        return this;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StockDailyTradeInfo stockDailyTradeInfo = (StockDailyTradeInfo) o;
        if (stockDailyTradeInfo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), stockDailyTradeInfo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StockDailyTradeInfo{" +
            "id=" + getId() +
            ", tradedDate='" + getTradedDate() + "'" +
            ", dayOpen=" + getDayOpen() +
            ", dayClose=" + getDayClose() +
            ", dayHigh=" + getDayHigh() +
            ", dayLow=" + getDayLow() +
            ", change=" + getChange() +
            ", changePercentage=" + getChangePercentage() +
            ", tradedVolume=" + getTradedVolume() +
            ", tradedValue=" + getTradedValue() +
            ", deliveryPercentage=" + getDeliveryPercentage() +
            ", yearHigh=" + getYearHigh() +
            ", yearLow=" + getYearLow() +
            "}";
    }
}
