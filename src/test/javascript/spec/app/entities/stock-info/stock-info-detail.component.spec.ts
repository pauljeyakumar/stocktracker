/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { StockTrackerTestModule } from '../../../test.module';
import { StockInfoDetailComponent } from '../../../../../../main/webapp/app/entities/stock-info/stock-info-detail.component';
import { StockInfoService } from '../../../../../../main/webapp/app/entities/stock-info/stock-info.service';
import { StockInfo } from '../../../../../../main/webapp/app/entities/stock-info/stock-info.model';

describe('Component Tests', () => {

    describe('StockInfo Management Detail Component', () => {
        let comp: StockInfoDetailComponent;
        let fixture: ComponentFixture<StockInfoDetailComponent>;
        let service: StockInfoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [StockTrackerTestModule],
                declarations: [StockInfoDetailComponent],
                providers: [
                    StockInfoService
                ]
            })
            .overrideTemplate(StockInfoDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(StockInfoDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StockInfoService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new StockInfo(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.stockInfo).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
