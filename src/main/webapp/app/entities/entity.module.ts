import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { StockTrackerStockModule } from './stock/stock.module';
import { StockTrackerStockInfoModule } from './stock-info/stock-info.module';
import { StockTrackerStockDailyTradeInfoModule } from './stock-daily-trade-info/stock-daily-trade-info.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        StockTrackerStockModule,
        StockTrackerStockInfoModule,
        StockTrackerStockDailyTradeInfoModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StockTrackerEntityModule {}
