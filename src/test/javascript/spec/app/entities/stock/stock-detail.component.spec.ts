/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { StockTrackerTestModule } from '../../../test.module';
import { StockDetailComponent } from '../../../../../../main/webapp/app/entities/stock/stock-detail.component';
import { StockService } from '../../../../../../main/webapp/app/entities/stock/stock.service';
import { Stock } from '../../../../../../main/webapp/app/entities/stock/stock.model';

describe('Component Tests', () => {

    describe('Stock Management Detail Component', () => {
        let comp: StockDetailComponent;
        let fixture: ComponentFixture<StockDetailComponent>;
        let service: StockService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [StockTrackerTestModule],
                declarations: [StockDetailComponent],
                providers: [
                    StockService
                ]
            })
            .overrideTemplate(StockDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(StockDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StockService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Stock(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.stock).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
