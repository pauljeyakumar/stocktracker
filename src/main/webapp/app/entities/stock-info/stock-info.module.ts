import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { StockTrackerSharedModule } from '../../shared';
import {
    StockInfoService,
    StockInfoPopupService,
    StockInfoComponent,
    StockInfoDetailComponent,
    StockInfoDialogComponent,
    StockInfoPopupComponent,
    StockInfoDeletePopupComponent,
    StockInfoDeleteDialogComponent,
    stockInfoRoute,
    stockInfoPopupRoute,
} from './';

const ENTITY_STATES = [
    ...stockInfoRoute,
    ...stockInfoPopupRoute,
];

@NgModule({
    imports: [
        StockTrackerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        StockInfoComponent,
        StockInfoDetailComponent,
        StockInfoDialogComponent,
        StockInfoDeleteDialogComponent,
        StockInfoPopupComponent,
        StockInfoDeletePopupComponent,
    ],
    entryComponents: [
        StockInfoComponent,
        StockInfoDialogComponent,
        StockInfoPopupComponent,
        StockInfoDeleteDialogComponent,
        StockInfoDeletePopupComponent,
    ],
    providers: [
        StockInfoService,
        StockInfoPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StockTrackerStockInfoModule {}
