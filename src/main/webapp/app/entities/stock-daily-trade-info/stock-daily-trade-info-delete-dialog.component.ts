import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { StockDailyTradeInfo } from './stock-daily-trade-info.model';
import { StockDailyTradeInfoPopupService } from './stock-daily-trade-info-popup.service';
import { StockDailyTradeInfoService } from './stock-daily-trade-info.service';

@Component({
    selector: 'jhi-stock-daily-trade-info-delete-dialog',
    templateUrl: './stock-daily-trade-info-delete-dialog.component.html'
})
export class StockDailyTradeInfoDeleteDialogComponent {

    stockDailyTradeInfo: StockDailyTradeInfo;

    constructor(
        private stockDailyTradeInfoService: StockDailyTradeInfoService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.stockDailyTradeInfoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'stockDailyTradeInfoListModification',
                content: 'Deleted an stockDailyTradeInfo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-stock-daily-trade-info-delete-popup',
    template: ''
})
export class StockDailyTradeInfoDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private stockDailyTradeInfoPopupService: StockDailyTradeInfoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.stockDailyTradeInfoPopupService
                .open(StockDailyTradeInfoDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
