import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { StockTrackerSharedModule } from '../../shared';
import {
    StockDailyTradeInfoService,
    StockDailyTradeInfoPopupService,
    StockDailyTradeInfoComponent,
    StockDailyTradeInfoDetailComponent,
    StockDailyTradeInfoDialogComponent,
    StockDailyTradeInfoPopupComponent,
    StockDailyTradeInfoDeletePopupComponent,
    StockDailyTradeInfoDeleteDialogComponent,
    stockDailyTradeInfoRoute,
    stockDailyTradeInfoPopupRoute,
} from './';

const ENTITY_STATES = [
    ...stockDailyTradeInfoRoute,
    ...stockDailyTradeInfoPopupRoute,
];

@NgModule({
    imports: [
        StockTrackerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        StockDailyTradeInfoComponent,
        StockDailyTradeInfoDetailComponent,
        StockDailyTradeInfoDialogComponent,
        StockDailyTradeInfoDeleteDialogComponent,
        StockDailyTradeInfoPopupComponent,
        StockDailyTradeInfoDeletePopupComponent,
    ],
    entryComponents: [
        StockDailyTradeInfoComponent,
        StockDailyTradeInfoDialogComponent,
        StockDailyTradeInfoPopupComponent,
        StockDailyTradeInfoDeleteDialogComponent,
        StockDailyTradeInfoDeletePopupComponent,
    ],
    providers: [
        StockDailyTradeInfoService,
        StockDailyTradeInfoPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StockTrackerStockDailyTradeInfoModule {}
