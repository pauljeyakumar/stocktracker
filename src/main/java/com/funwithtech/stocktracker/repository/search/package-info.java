/**
 * Spring Data Elasticsearch repositories.
 */
package com.funwithtech.stocktracker.repository.search;
