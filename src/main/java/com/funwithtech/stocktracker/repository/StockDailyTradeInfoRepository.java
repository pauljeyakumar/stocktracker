package com.funwithtech.stocktracker.repository;

import com.funwithtech.stocktracker.domain.StockDailyTradeInfo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the StockDailyTradeInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface StockDailyTradeInfoRepository extends JpaRepository<StockDailyTradeInfo, Long> {

}
