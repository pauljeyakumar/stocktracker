package com.funwithtech.stocktracker.web.rest;

import com.funwithtech.stocktracker.StockTrackerApp;

import com.funwithtech.stocktracker.domain.StockInfo;
import com.funwithtech.stocktracker.repository.StockInfoRepository;
import com.funwithtech.stocktracker.repository.search.StockInfoSearchRepository;
import com.funwithtech.stocktracker.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.funwithtech.stocktracker.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.funwithtech.stocktracker.domain.enumeration.Sector;
/**
 * Test class for the StockInfoResource REST controller.
 *
 * @see StockInfoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = StockTrackerApp.class)
public class StockInfoResourceIntTest {

    private static final Sector DEFAULT_SECTOR = Sector.AUTO;
    private static final Sector UPDATED_SECTOR = Sector.BANK;

    private static final Float DEFAULT_EARNING_PER_STOCK = 1F;
    private static final Float UPDATED_EARNING_PER_STOCK = 2F;

    private static final Float DEFAULT_DIVIDENT = 1F;
    private static final Float UPDATED_DIVIDENT = 2F;

    private static final Integer DEFAULT_FACE_VALUE = 1;
    private static final Integer UPDATED_FACE_VALUE = 2;

    private static final Float DEFAULT_BOOK_VALUE = 1F;
    private static final Float UPDATED_BOOK_VALUE = 2F;

    @Autowired
    private StockInfoRepository stockInfoRepository;

    @Autowired
    private StockInfoSearchRepository stockInfoSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restStockInfoMockMvc;

    private StockInfo stockInfo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final StockInfoResource stockInfoResource = new StockInfoResource(stockInfoRepository, stockInfoSearchRepository);
        this.restStockInfoMockMvc = MockMvcBuilders.standaloneSetup(stockInfoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StockInfo createEntity(EntityManager em) {
        StockInfo stockInfo = new StockInfo()
            .sector(DEFAULT_SECTOR)
            .earningPerStock(DEFAULT_EARNING_PER_STOCK)
            .divident(DEFAULT_DIVIDENT)
            .faceValue(DEFAULT_FACE_VALUE)
            .bookValue(DEFAULT_BOOK_VALUE);
        return stockInfo;
    }

    @Before
    public void initTest() {
        stockInfoSearchRepository.deleteAll();
        stockInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void createStockInfo() throws Exception {
        int databaseSizeBeforeCreate = stockInfoRepository.findAll().size();

        // Create the StockInfo
        restStockInfoMockMvc.perform(post("/api/stock-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stockInfo)))
            .andExpect(status().isCreated());

        // Validate the StockInfo in the database
        List<StockInfo> stockInfoList = stockInfoRepository.findAll();
        assertThat(stockInfoList).hasSize(databaseSizeBeforeCreate + 1);
        StockInfo testStockInfo = stockInfoList.get(stockInfoList.size() - 1);
        assertThat(testStockInfo.getSector()).isEqualTo(DEFAULT_SECTOR);
        assertThat(testStockInfo.getEarningPerStock()).isEqualTo(DEFAULT_EARNING_PER_STOCK);
        assertThat(testStockInfo.getDivident()).isEqualTo(DEFAULT_DIVIDENT);
        assertThat(testStockInfo.getFaceValue()).isEqualTo(DEFAULT_FACE_VALUE);
        assertThat(testStockInfo.getBookValue()).isEqualTo(DEFAULT_BOOK_VALUE);

        // Validate the StockInfo in Elasticsearch
        StockInfo stockInfoEs = stockInfoSearchRepository.findOne(testStockInfo.getId());
        assertThat(stockInfoEs).isEqualToIgnoringGivenFields(testStockInfo);
    }

    @Test
    @Transactional
    public void createStockInfoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = stockInfoRepository.findAll().size();

        // Create the StockInfo with an existing ID
        stockInfo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStockInfoMockMvc.perform(post("/api/stock-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stockInfo)))
            .andExpect(status().isBadRequest());

        // Validate the StockInfo in the database
        List<StockInfo> stockInfoList = stockInfoRepository.findAll();
        assertThat(stockInfoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkSectorIsRequired() throws Exception {
        int databaseSizeBeforeTest = stockInfoRepository.findAll().size();
        // set the field null
        stockInfo.setSector(null);

        // Create the StockInfo, which fails.

        restStockInfoMockMvc.perform(post("/api/stock-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stockInfo)))
            .andExpect(status().isBadRequest());

        List<StockInfo> stockInfoList = stockInfoRepository.findAll();
        assertThat(stockInfoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEarningPerStockIsRequired() throws Exception {
        int databaseSizeBeforeTest = stockInfoRepository.findAll().size();
        // set the field null
        stockInfo.setEarningPerStock(null);

        // Create the StockInfo, which fails.

        restStockInfoMockMvc.perform(post("/api/stock-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stockInfo)))
            .andExpect(status().isBadRequest());

        List<StockInfo> stockInfoList = stockInfoRepository.findAll();
        assertThat(stockInfoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFaceValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = stockInfoRepository.findAll().size();
        // set the field null
        stockInfo.setFaceValue(null);

        // Create the StockInfo, which fails.

        restStockInfoMockMvc.perform(post("/api/stock-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stockInfo)))
            .andExpect(status().isBadRequest());

        List<StockInfo> stockInfoList = stockInfoRepository.findAll();
        assertThat(stockInfoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBookValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = stockInfoRepository.findAll().size();
        // set the field null
        stockInfo.setBookValue(null);

        // Create the StockInfo, which fails.

        restStockInfoMockMvc.perform(post("/api/stock-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stockInfo)))
            .andExpect(status().isBadRequest());

        List<StockInfo> stockInfoList = stockInfoRepository.findAll();
        assertThat(stockInfoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllStockInfos() throws Exception {
        // Initialize the database
        stockInfoRepository.saveAndFlush(stockInfo);

        // Get all the stockInfoList
        restStockInfoMockMvc.perform(get("/api/stock-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(stockInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].sector").value(hasItem(DEFAULT_SECTOR.toString())))
            .andExpect(jsonPath("$.[*].earningPerStock").value(hasItem(DEFAULT_EARNING_PER_STOCK.doubleValue())))
            .andExpect(jsonPath("$.[*].divident").value(hasItem(DEFAULT_DIVIDENT.doubleValue())))
            .andExpect(jsonPath("$.[*].faceValue").value(hasItem(DEFAULT_FACE_VALUE)))
            .andExpect(jsonPath("$.[*].bookValue").value(hasItem(DEFAULT_BOOK_VALUE.doubleValue())));
    }

    @Test
    @Transactional
    public void getStockInfo() throws Exception {
        // Initialize the database
        stockInfoRepository.saveAndFlush(stockInfo);

        // Get the stockInfo
        restStockInfoMockMvc.perform(get("/api/stock-infos/{id}", stockInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(stockInfo.getId().intValue()))
            .andExpect(jsonPath("$.sector").value(DEFAULT_SECTOR.toString()))
            .andExpect(jsonPath("$.earningPerStock").value(DEFAULT_EARNING_PER_STOCK.doubleValue()))
            .andExpect(jsonPath("$.divident").value(DEFAULT_DIVIDENT.doubleValue()))
            .andExpect(jsonPath("$.faceValue").value(DEFAULT_FACE_VALUE))
            .andExpect(jsonPath("$.bookValue").value(DEFAULT_BOOK_VALUE.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingStockInfo() throws Exception {
        // Get the stockInfo
        restStockInfoMockMvc.perform(get("/api/stock-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStockInfo() throws Exception {
        // Initialize the database
        stockInfoRepository.saveAndFlush(stockInfo);
        stockInfoSearchRepository.save(stockInfo);
        int databaseSizeBeforeUpdate = stockInfoRepository.findAll().size();

        // Update the stockInfo
        StockInfo updatedStockInfo = stockInfoRepository.findOne(stockInfo.getId());
        // Disconnect from session so that the updates on updatedStockInfo are not directly saved in db
        em.detach(updatedStockInfo);
        updatedStockInfo
            .sector(UPDATED_SECTOR)
            .earningPerStock(UPDATED_EARNING_PER_STOCK)
            .divident(UPDATED_DIVIDENT)
            .faceValue(UPDATED_FACE_VALUE)
            .bookValue(UPDATED_BOOK_VALUE);

        restStockInfoMockMvc.perform(put("/api/stock-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedStockInfo)))
            .andExpect(status().isOk());

        // Validate the StockInfo in the database
        List<StockInfo> stockInfoList = stockInfoRepository.findAll();
        assertThat(stockInfoList).hasSize(databaseSizeBeforeUpdate);
        StockInfo testStockInfo = stockInfoList.get(stockInfoList.size() - 1);
        assertThat(testStockInfo.getSector()).isEqualTo(UPDATED_SECTOR);
        assertThat(testStockInfo.getEarningPerStock()).isEqualTo(UPDATED_EARNING_PER_STOCK);
        assertThat(testStockInfo.getDivident()).isEqualTo(UPDATED_DIVIDENT);
        assertThat(testStockInfo.getFaceValue()).isEqualTo(UPDATED_FACE_VALUE);
        assertThat(testStockInfo.getBookValue()).isEqualTo(UPDATED_BOOK_VALUE);

        // Validate the StockInfo in Elasticsearch
        StockInfo stockInfoEs = stockInfoSearchRepository.findOne(testStockInfo.getId());
        assertThat(stockInfoEs).isEqualToIgnoringGivenFields(testStockInfo);
    }

    @Test
    @Transactional
    public void updateNonExistingStockInfo() throws Exception {
        int databaseSizeBeforeUpdate = stockInfoRepository.findAll().size();

        // Create the StockInfo

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restStockInfoMockMvc.perform(put("/api/stock-infos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(stockInfo)))
            .andExpect(status().isCreated());

        // Validate the StockInfo in the database
        List<StockInfo> stockInfoList = stockInfoRepository.findAll();
        assertThat(stockInfoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteStockInfo() throws Exception {
        // Initialize the database
        stockInfoRepository.saveAndFlush(stockInfo);
        stockInfoSearchRepository.save(stockInfo);
        int databaseSizeBeforeDelete = stockInfoRepository.findAll().size();

        // Get the stockInfo
        restStockInfoMockMvc.perform(delete("/api/stock-infos/{id}", stockInfo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean stockInfoExistsInEs = stockInfoSearchRepository.exists(stockInfo.getId());
        assertThat(stockInfoExistsInEs).isFalse();

        // Validate the database is empty
        List<StockInfo> stockInfoList = stockInfoRepository.findAll();
        assertThat(stockInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchStockInfo() throws Exception {
        // Initialize the database
        stockInfoRepository.saveAndFlush(stockInfo);
        stockInfoSearchRepository.save(stockInfo);

        // Search the stockInfo
        restStockInfoMockMvc.perform(get("/api/_search/stock-infos?query=id:" + stockInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(stockInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].sector").value(hasItem(DEFAULT_SECTOR.toString())))
            .andExpect(jsonPath("$.[*].earningPerStock").value(hasItem(DEFAULT_EARNING_PER_STOCK.doubleValue())))
            .andExpect(jsonPath("$.[*].divident").value(hasItem(DEFAULT_DIVIDENT.doubleValue())))
            .andExpect(jsonPath("$.[*].faceValue").value(hasItem(DEFAULT_FACE_VALUE)))
            .andExpect(jsonPath("$.[*].bookValue").value(hasItem(DEFAULT_BOOK_VALUE.doubleValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(StockInfo.class);
        StockInfo stockInfo1 = new StockInfo();
        stockInfo1.setId(1L);
        StockInfo stockInfo2 = new StockInfo();
        stockInfo2.setId(stockInfo1.getId());
        assertThat(stockInfo1).isEqualTo(stockInfo2);
        stockInfo2.setId(2L);
        assertThat(stockInfo1).isNotEqualTo(stockInfo2);
        stockInfo1.setId(null);
        assertThat(stockInfo1).isNotEqualTo(stockInfo2);
    }
}
