import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { StockInfoComponent } from './stock-info.component';
import { StockInfoDetailComponent } from './stock-info-detail.component';
import { StockInfoPopupComponent } from './stock-info-dialog.component';
import { StockInfoDeletePopupComponent } from './stock-info-delete-dialog.component';

export const stockInfoRoute: Routes = [
    {
        path: 'stock-info',
        component: StockInfoComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'stockTrackerApp.stockInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'stock-info/:id',
        component: StockInfoDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'stockTrackerApp.stockInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const stockInfoPopupRoute: Routes = [
    {
        path: 'stock-info-new',
        component: StockInfoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'stockTrackerApp.stockInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stock-info/:id/edit',
        component: StockInfoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'stockTrackerApp.stockInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'stock-info/:id/delete',
        component: StockInfoDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'stockTrackerApp.stockInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
