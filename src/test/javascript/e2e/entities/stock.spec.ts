import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Stock e2e test', () => {

    let navBarPage: NavBarPage;
    let stockDialogPage: StockDialogPage;
    let stockComponentsPage: StockComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Stocks', () => {
        navBarPage.goToEntity('stock');
        stockComponentsPage = new StockComponentsPage();
        expect(stockComponentsPage.getTitle())
            .toMatch(/stockTrackerApp.stock.home.title/);

    });

    it('should load create Stock dialog', () => {
        stockComponentsPage.clickOnCreateButton();
        stockDialogPage = new StockDialogPage();
        expect(stockDialogPage.getModalTitle())
            .toMatch(/stockTrackerApp.stock.home.createOrEditLabel/);
        stockDialogPage.close();
    });

    it('should create and save Stocks', () => {
        stockComponentsPage.clickOnCreateButton();
        stockDialogPage.setStockCodeInput('stockCode');
        expect(stockDialogPage.getStockCodeInput()).toMatch('stockCode');
        stockDialogPage.setStockNameInput('stockName');
        expect(stockDialogPage.getStockNameInput()).toMatch('stockName');
        stockDialogPage.exchangeSelectLastOption();
        stockDialogPage.save();
        expect(stockDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class StockComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-stock div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class StockDialogPage {
    modalTitle = element(by.css('h4#myStockLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    stockCodeInput = element(by.css('input#field_stockCode'));
    stockNameInput = element(by.css('input#field_stockName'));
    exchangeSelect = element(by.css('select#field_exchange'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setStockCodeInput = function(stockCode) {
        this.stockCodeInput.sendKeys(stockCode);
    };

    getStockCodeInput = function() {
        return this.stockCodeInput.getAttribute('value');
    };

    setStockNameInput = function(stockName) {
        this.stockNameInput.sendKeys(stockName);
    };

    getStockNameInput = function() {
        return this.stockNameInput.getAttribute('value');
    };

    setExchangeSelect = function(exchange) {
        this.exchangeSelect.sendKeys(exchange);
    };

    getExchangeSelect = function() {
        return this.exchangeSelect.element(by.css('option:checked')).getText();
    };

    exchangeSelectLastOption = function() {
        this.exchangeSelect.all(by.tagName('option')).last().click();
    };
    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
