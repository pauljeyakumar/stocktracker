import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { StockDailyTradeInfo } from './stock-daily-trade-info.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<StockDailyTradeInfo>;

@Injectable()
export class StockDailyTradeInfoService {

    private resourceUrl =  SERVER_API_URL + 'api/stock-daily-trade-infos';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/stock-daily-trade-infos';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(stockDailyTradeInfo: StockDailyTradeInfo): Observable<EntityResponseType> {
        const copy = this.convert(stockDailyTradeInfo);
        return this.http.post<StockDailyTradeInfo>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(stockDailyTradeInfo: StockDailyTradeInfo): Observable<EntityResponseType> {
        const copy = this.convert(stockDailyTradeInfo);
        return this.http.put<StockDailyTradeInfo>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<StockDailyTradeInfo>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<StockDailyTradeInfo[]>> {
        const options = createRequestOption(req);
        return this.http.get<StockDailyTradeInfo[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<StockDailyTradeInfo[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<StockDailyTradeInfo[]>> {
        const options = createRequestOption(req);
        return this.http.get<StockDailyTradeInfo[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<StockDailyTradeInfo[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: StockDailyTradeInfo = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<StockDailyTradeInfo[]>): HttpResponse<StockDailyTradeInfo[]> {
        const jsonResponse: StockDailyTradeInfo[] = res.body;
        const body: StockDailyTradeInfo[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to StockDailyTradeInfo.
     */
    private convertItemFromServer(stockDailyTradeInfo: StockDailyTradeInfo): StockDailyTradeInfo {
        const copy: StockDailyTradeInfo = Object.assign({}, stockDailyTradeInfo);
        copy.tradedDate = this.dateUtils
            .convertLocalDateFromServer(stockDailyTradeInfo.tradedDate);
        return copy;
    }

    /**
     * Convert a StockDailyTradeInfo to a JSON which can be sent to the server.
     */
    private convert(stockDailyTradeInfo: StockDailyTradeInfo): StockDailyTradeInfo {
        const copy: StockDailyTradeInfo = Object.assign({}, stockDailyTradeInfo);
        copy.tradedDate = this.dateUtils
            .convertLocalDateToServer(stockDailyTradeInfo.tradedDate);
        return copy;
    }
}
